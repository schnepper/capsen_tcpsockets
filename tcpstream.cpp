/*
  TCPStream.h

  TCPStream class definition. TCPStream provides methods to trasnfer
  data between peers over a TCP/IP connection.

  ------------------------------------------

  Copyright � 2013 [Vic Hargrave - http://vichargrave.com]

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#include <arpa/inet.h>
#include <errno.h>
#include <iostream>
#include <netinet/tcp.h>
#include <string.h>
#include "tcpstream.h"

TCPStream::TCPStream(int sd, struct sockaddr_in* address) : m_sd(sd)
{
  char ip[50];
  inet_ntop(PF_INET, (struct in_addr*)&(address->sin_addr.s_addr), ip, sizeof(ip)-1);
  m_peerIP = ip;
  m_peerPort = ntohs(address->sin_port);
}

TCPStream::~TCPStream()
{
  close(m_sd);
}

void TCPStream::close_stream()
{
  if (m_sd >= 0) {
    std::cout << "closing sd " << m_sd << std::endl;
    // shutdown causes a pending read to finish, close does not.
    shutdown(m_sd, SHUT_RDWR);
    // close(m_sd);
    m_sd = -1;
  }
}

ssize_t TCPStream::send(void* buffer, size_t len)
{
  return write(m_sd, buffer, len);
}

ssize_t TCPStream::send(const string buffer, size_t len)
{
  return write(m_sd, buffer.c_str(), (len == -1 ? buffer.size() : len));
}

ssize_t TCPStream::receive(void* buffer, size_t len, int timeout)
{
  if (timeout <= 0) {
    return read(m_sd, buffer, len);
  }

  if (waitForReadEvent(timeout) == true) {
    return read(m_sd, buffer, len);
  }
  return connectionTimedOut;
}

string TCPStream::getPeerIP()
{
  return m_peerIP;
}

int TCPStream::getPeerPort()
{
  return m_peerPort;
}

// dbug
void TCPStream::showKeepaliveSettings()
{
  int optval;
  socklen_t optlen = sizeof(optval);
  getsockopt(m_sd, SOL_SOCKET, SO_KEEPALIVE, &optval, &optlen);
  std::cout << "SO_KEEPALIVE is " << (optval ? "ON" : "OFF") << std::endl;
  getsockopt(m_sd, IPPROTO_TCP, TCP_KEEPIDLE, &optval, &optlen);
  std::cout << "TCP_KEEPIDLE is " << optval << std::endl;
  getsockopt(m_sd, IPPROTO_TCP, TCP_KEEPINTVL, &optval, &optlen);
  std::cout << "TCP_KEEPINTVL is " << optval << std::endl;
  getsockopt(m_sd, IPPROTO_TCP, TCP_KEEPCNT, &optval, &optlen);
  std::cout << "TCP_KEEPCNT is " << optval << std::endl;
  getsockopt(m_sd, IPPROTO_TCP, TCP_USER_TIMEOUT, &optval, &optlen);
  std::cout << "TCP_USER_TIMEOUT is " << optval << std::endl;
}

void TCPStream::setKeepaliveParams(const KeepaliveParams& params)
{
  int optval;
  socklen_t optlen = sizeof(optval);

  // showKeepaliveSettings(); // dbug

  if (!params.keepalive_enabled) {
    return;
  }

  // enable keepalive
  optval = 1;
  if (setsockopt(m_sd, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen) < 0) {
    int er = errno;
    std::cout << "Error enabling keepalive. setsockopt() error"
              << er << " " << strerror(er) << std::endl;
    return;
  }

  if (params.keepalive_idle_secs != -1) {
    optval = params.keepalive_idle_secs;
    // std::cout << "Setting keep_idle_secs to: " << optval << std::endl;
    if (setsockopt(m_sd, IPPROTO_TCP, TCP_KEEPIDLE, &optval, optlen) < 0) {
      int er = errno;
      std::cout << "Error setting keep_idle_secs: " << er << " " << strerror(er) << std::endl;
    }
  }

  if (params.keepalive_interval_secs != -1) {
    optval = params.keepalive_interval_secs;
    // std::cout << "Setting keep_interval_secs to: " << optval << std::endl;
    if (setsockopt(m_sd, IPPROTO_TCP, TCP_KEEPINTVL, &optval, optlen) < 0) {
      int er = errno;
      std::cout << "Error setting keep_interval_secs" << er << " " << strerror(er) << std::endl;
    }
  }

  if (params.keepalive_probes_cnt != -1) {
    optval = params.keepalive_probes_cnt;
    // std::cout << "Setting keep_probes_cnt to: " << optval << std::endl;
    if (setsockopt(m_sd, IPPROTO_TCP, TCP_KEEPCNT, &optval, optlen) < 0) {
      int er = errno;
      std::cout << "Error setting keep_probes_cnt" << er << " " << strerror(er) << std::endl;
    }
  }

  if (params.tcp_user_timeout != -1) {
    optval = params.tcp_user_timeout;
    // std::cout << "Setting tcp_user_timeout to: " << optval << std::endl;
    if (setsockopt(m_sd, IPPROTO_TCP, TCP_USER_TIMEOUT, &optval, optlen) < 0) {
      int er = errno;
      std::cout << "Error setting tcp_user_timeout" << er << " " << strerror(er) << std::endl;
    }
  }

  // showKeepaliveSettings(); // dbug
}

bool TCPStream::waitForReadEvent(int timeout)
{
  fd_set sdset;
  struct timeval tv;

  tv.tv_sec = timeout;
  tv.tv_usec = 0;
  FD_ZERO(&sdset);
  FD_SET(m_sd, &sdset);
  if (select(m_sd + 1, &sdset, NULL, NULL, &tv) > 0) {
    return true;
  }
  return false;
}
